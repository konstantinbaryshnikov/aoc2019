const {amplifier} = require('../common/amplifier');
const {permute} = require('../common/permutator');
const {program} = require('./input');

const phasePermutations = permute([0,1,2,3,4]);
const amplifiedSignals = phasePermutations.map(phases => amplifier(program)(phases, 0));
console.log(Math.max(...amplifiedSignals));
