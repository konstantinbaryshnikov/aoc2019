const {feedbackLoop} = require('../common/amplifier');
const {permute} = require('../common/permutator');
const {program} = require('./input');

const phasePermutations = permute([5,6,7,8,9]);
const amplifiedSignals = phasePermutations.map(phases => {
    return feedbackLoop(program)(phases, 0);
});

console.log(Math.max(...amplifiedSignals));
