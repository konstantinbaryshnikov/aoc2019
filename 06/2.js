const {parseInput} = require('../common/orbit/inputParser');
const {distance} = require('../common/orbit');
const {mapInput} = require('./input');

console.log(distance(parseInput(mapInput), 'YOU', 'SAN'));
