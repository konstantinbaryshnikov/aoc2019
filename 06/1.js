const {parseInput} = require('../common/orbit/inputParser');
const {calcCrc} = require('../common/orbit');
const {mapInput} = require('./input');

console.log(calcCrc(parseInput(mapInput)));
