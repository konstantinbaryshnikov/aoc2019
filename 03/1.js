const {Panel} = require('./panel');
const {wires} = require('./input');
const {expectEq} = require('../common/tests');

const panel = new Panel();
wires.forEach(wire => panel.addWire(wire));
console.log(panel.closestIntersection());

