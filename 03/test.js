const {Panel} = require('./panel');
const {wires} = require('./input');
const {expectEq} = require('../common/tests');

function testPanelClosestIntersection() {
    const testCases = [
        [
            6,
            [
                'R8,U5,L5,D3'.split(','), 
                'U7,R6,D4,L4'.split(','),
            ],
            20
        ],
        [
            159,
            [
                'R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','), 
                'U62,R66,U55,R34,D71,R55,D58,R83'.split(','),
            ],
            500
        ],
        [
            135,
            [
                'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(','), 
                'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(','),
            ],
            200
        ],
    ];
    testCases.forEach(testcase => {
        const panel = new Panel(testcase[2], testcase[2]);
        testcase[1].forEach(wire => panel.addWire(wire));
        expectEq(testcase[0], panel.closestIntersection(), 'closest intersection');
    });
}

function testPanelMinSignalDelayIntersection() {
    const testCases = [
        [
            30,
            [
                'R8,U5,L5,D3'.split(','), 
                'U7,R6,D4,L4'.split(','),
            ],
            20
        ],
        [
            610,
            [
                'R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','), 
                'U62,R66,U55,R34,D71,R55,D58,R83'.split(','),
            ],
            500
        ],
        [
            410,
            [
                'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51'.split(','), 
                'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'.split(','),
            ],
            200
        ],
    ];
    testCases.forEach(testcase => {
        const panel = new Panel(testcase[2], testcase[2]);
        testcase[1].forEach(wire => panel.addWire(wire));
        expectEq(testcase[0], panel.minSignalDelayIntersection(), 'closest intersection');
    });
}

testPanelClosestIntersection();
testPanelMinSignalDelayIntersection();
