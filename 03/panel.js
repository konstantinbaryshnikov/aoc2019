class Panel {

    constructor(width = 10000, height = 10000) {
        this.width = width;
        this.height = height;
        this.center = [ width >> 1, height >> 1 ];
        this.grid = [];
        this.intersections = [];
        this.wiresCount = 0;
        this.resetPosition();
    }

    resetPosition() {
        this.x = this.center[0];
        this.y = this.center[1];
    }

    closestIntersection() {
        const [cx, cy] = this.center;
        return this.intersections.reduce((min, [x, y]) => Math.min(min, Math.abs(cx - x) + Math.abs(cy - y)), Number.MAX_VALUE);
    }

    minSignalDelayIntersection() {
        return this.intersections.reduce((min, intersection) => Math.min(min, this.get(...intersection).reduce((c, v) => c + (v || 0))), Number.MAX_VALUE);
    }

    addWire(path) {
        this.grid.push([]);
        this.resetPosition();
        let pathLen = 0;
        for (let part of path) {
            const dir = part.charAt(0);
            let len = part.substr(1) | 0;
            while (len-- > 0) {
                this[dir]();
                this.set(this.wiresCount, ++pathLen);
                const usage = this.get();
                if (usage.filter(x => !!x).length > 1) {
                    this.intersections.push([this.x, this.y]);
                }
            }
        }
        this.wiresCount++;
    }

    get(x = this.x, y = this.y) {
        return this.grid[this.indexOf(x, y)] || [];
    }

    set(wireIndex, value, x = this.x, y = this.y) {
        const idx = this.indexOf(x, y);
        if (!this.grid[idx]) {
            this.grid[idx] = [];
        }
        this.grid[idx][wireIndex] = value;
    }

    indexOf(x, y) {
        return y * this.width + x;
    }

    toString() {
        let output = '';
        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                if (x === this.center[0] && y === this.center[1]) {
                    output += 'o';
                } else switch ((this.get(x, y) || []).filter(x => !!x).length) {
                    case 0: output += '.'; break;
                    case 1: output += '*'; break;
                    default: output += 'X'; break;
                }
            }
            output += "\n";
        }
        return output;
    }

    R() { this.x++; }
    L() { this.x--; }
    D() { this.y++; }
    U() { this.y--; }

}

module.exports = {Panel};
