const {machine} = require('../common/intcode');
const {program} = require('./input');

console.log(machine().repl(program)([2]));
