const {machine} = require('../common/intcode');
const {program} = require('./input');

console.log(machine(true).repl(program)([1]));
