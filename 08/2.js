const {input} = require('./input');
const w = 25, h = 6;
const layers = input.split(new RegExp(`(.{${w*h}})`)).filter(s => s.length);

for (let i = 0; i < w * h; i++) {
    if (i % w == 0) process.stdout.write("\n");
    const px = layers.map(layer => layer.charAt(i)).filter(c => c !== '2')[0];
    process.stdout.write(px === '1' ? '#' : '.');
}
