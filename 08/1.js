const {input} = require('./input');
const w = 25, h = 6;
const layers = input.split(new RegExp(`(.{${w*h}})`)).filter(s => s.length);
const zeroes = layers.map((layer, idx) => ({idx, zeroes: layer.match(/0/g).length}));
zeroes.sort((a, b) => a.zeroes - b.zeroes);
const layer = layers[zeroes[0].idx];
const n1s = layer.match(/1/g).length;
const n2s = layer.match(/2/g).length;
console.log(n1s * n2s);
