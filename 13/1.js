const {Arcade} = require('../common/arcade');
const {program} = require('./input');

const w = 80;
const h = 25;

const arcade = new Arcade(program, w, h);
arcade.step([]);

console.log(arcade.countOf(2));
