const {Arcade} = require('../common/arcade');
const {program} = require('./input');
const readline = require('readline');

const visDelay = +process.argv[2];
let interactive = false;
if (visDelay && !isNaN(visDelay)) {
    readline.cursorTo(process.stdout, 0, 0);
    readline.clearScreenDown(process.stdout);
    interactive = !!process.argv[3];
    if (interactive) {
        process.stdin.setRawMode(true);
    }
}

const w = 45;
const h = 23;
const arcade = new Arcade(program, w, h, 2);

function ai(arcade) {
    const ball = arcade.lastTile[4];
    const paddle = arcade.lastTile[3];

    if (!ball || !paddle) {
        return 0;
    }

    const dx = paddle[0] - ball[0];

    if (dx > 0) {
        return -1;
    } else if (dx < 0) {
        return 1;
    } else {
        return 0;
    }
}

async function run() {
    let halt = false;

    let score;
    let dump = [];
    let joystickPos = 0;
    let win = false;

    interactive && process.stdin.on('data', input => {
        switch (input[0]) {
            case 32: // space
                joystickPos = 0; break;
            case 106: // j
                joystickPos = -1; break;
            case 107: // k
                joystickPos = 1; break;
            case 3: // ^C
                process.exit();
        }
    });

    while (!halt) {
        if (!interactive) {
            joystickPos = ai(arcade);
        }

        score = arcade.score;
        if (arcade.step([joystickPos])) {
            if (arcade.score) {
                score = arcade.score;
                win = true;
            }
            halt = true;
        }

        if (visDelay && !isNaN(visDelay)) {
            readline.cursorTo(process.stdout, 0, 0);
            readline.clearScreenDown(process.stdout);
            process.stdout.write(arcade.toString());
            await new Promise(resolve => setTimeout(resolve, visDelay));
        }
    }

    console.log(win ? 'YOU WIN!' : 'GAME OVER');
    console.log('Score:', score);
}

run().then(() => {
    process.exit();
});

