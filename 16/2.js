const {mfftPartial, parse} = require('../common/fft');
const {input} = require('./input');

const fullInput = parse(input.repeat(10000));
const offset = input.substr(0, 7) | 0;

const phases = 100;
const len = 8;

const result = mfftPartial(fullInput, phases, offset).slice(offset, offset + len).join('');
console.log(result);
