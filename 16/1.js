const {mfft, parse} = require('../common/fft');
const {input} = require('./input');

const phases = 100;
const len = 8;

const result = mfft(parse(input), phases).slice(0, len).join('');

console.log(result);
