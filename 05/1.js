const {machine} = require('../common/intcode');
const {program} = require('./input');

const input = [1];
try {
    console.log(machine()(program, input));
} catch (e) {
    console.log(e);
}
