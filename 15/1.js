const {MazeWalker} = require('../common/maze');
const readline = require('readline');

const mazeWalker = new MazeWalker();

const debug = !!process.argv[2];
const discover = !!process.argv[3];

if (debug) {
    readline.cursorTo(process.stdout, 0, 0);
    readline.clearScreenDown(process.stdout);
    mazeWalker.debug = (out) => {
        readline.cursorTo(process.stdout, 0, 0);
        process.stdout.write(out);
    };
}

if (discover) {
    mazeWalker.discover();
} else {
    const location = mazeWalker.findGoal();
    console.log(mazeWalker.getPathTo(location).length - 1);
}

