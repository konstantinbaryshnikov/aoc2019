const {MazeWalker} = require('../common/maze');
const readline = require('readline');
const spawn = require('child_process');

const mazeWalker = new MazeWalker();

const debug = !!process.argv[2];
const delay = !!process.argv[3];

mazeWalker.discover();

if (debug) {
    readline.cursorTo(process.stdout, 0, 0);
    readline.clearScreenDown(process.stdout);
    mazeWalker.debug = (out, steps) => {
        if (out === null) {
            console.log(steps);
            delay && spawn.execSync('sleep 0.1');
            return;
        }
        readline.cursorTo(process.stdout, 0, 0);
        process.stdout.write(out);
    };
}

const steps = mazeWalker.fill();
console.log(steps);
