const {Ascii} = require('../common/ascii');

const ascii = new Ascii();
ascii.loadMap();
const result = ascii.go();
console.log(result);
