const {Ascii} = require('../common/ascii');

const ascii = new Ascii();
ascii.loadMap();
const sum = ascii.calcAlignmentSum(ascii.findIntersections());
console.log(sum);
