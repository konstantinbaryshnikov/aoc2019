const {masses} = require('./inputs');
const {expectEq} = require('../common/tests');

function fuelRequired(mass) {
    return Math.floor(mass / 3) - 2;
}

function calcSumFuelRequirements() {
    return masses.map(mass => fuelRequired(mass)).reduce((a, c) => a + c, 0);
}

function testFuelRequired() {
    const testcases = [
        [12, 2],
        [14, 2],
        [1969, 654],
        [100756, 33583]
    ];
    testcases.forEach(testcase => expectEq(fuelRequired(testcase[0]), testcase[1], 'fuelRequired'));
}

testFuelRequired();

console.log(calcSumFuelRequirements());

