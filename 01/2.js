const {masses} = require('./inputs');
const {expectEq} = require('../common/tests');

function fuelRequired(mass) {
    const fuelMass = Math.floor(mass / 3) - 2;
    return fuelMass > 0 ? fuelMass + fuelRequired(fuelMass) : 0;
}

function calcSumFuelRequirements() {
    return masses.map(mass => fuelRequired(mass)).reduce((a, c) => a + c, 0);
}

function testFuelRequired() {
    const testcases = [
        [14, 2],
        [1969, 966],
        [100756, 50346]
    ];
    testcases.forEach(testcase => expectEq(fuelRequired(testcase[0]), testcase[1], 'fuelRequired'));
}

testFuelRequired();

console.log(calcSumFuelRequirements());

