function toDigits(x) {
    let result = [];
    while (x > 0) {
        result.unshift(x % 10);
        x = (x / 10) | 0;
    }
    return result;
}

function gcd(a, b, ...values) {
    while (b) {
        let tmp = b;
        b = a % b;
        a = tmp;
    }
    if (values.length) {
        return gcd(a, ...values);
    }
    return a;
}

function lcm(...values) {
    return values.reduce((a, b) => (a * b) / gcd(a, b));
}

module.exports = {
    toDigits,
    gcd,
    lcm,
};
