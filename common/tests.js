function expectEq(expected, actual, message = '') {
    console.assert(expected === actual, `Expected ${expected}, got ${actual} [${message}]`);
}

function expectEqs(expected, actual, message = '') {
    const e = JSON.stringify(expected), a = JSON.stringify(actual);
    console.assert(e === a, `Expected ${e}, got ${a} [${message}]`);
}

function expectTrue(value, message = '') {
    console.assert(!!value, `Expected true, got ${value} [${message}]`);
}

function expectFalse(value, message = '') {
    console.assert(!value, `Expected false, got ${value} [${message}]`);
}

function expectIs(expected, actual, message = '') {
    if (expected) {
        expectTrue(actual, message);
    } else {
        expectFalse(actual, message);
    }
}

module.exports = {
    expectEq,
    expectEqs,
    expectTrue,
    expectFalse,
    expectIs,
}
