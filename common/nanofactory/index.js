function parseChemicalUnit(chemicalUnit) {
    const [units, name] = chemicalUnit.trim().split(/\s+/);
    return {name, units: units | 0};
}

function parseInput(input) {
    return input.trim().split('\n').filter(s => s.trim().length).reduce((carry, line) => {
        const [inputsCu, outputCu] = line.split(/\s*=>\s*/);
        const inputs = inputsCu.split(/,\s*/).map(u => parseChemicalUnit(u));
        const output = parseChemicalUnit(outputCu);
        if (carry[output.name]) {
            throw new Error("Duplicate output: " + output.name);
        }
        carry[output.name] = {units: output.units, requires: inputs};
        return carry;
    }, {});
}

class Nanofactory {

    static fromInput(input, oreLimit) {
        return new this(parseInput(input), oreLimit);
    }

    constructor(formulas) {
        this.formulas = formulas;
    }

    reset() {
        this.leftovers = Object.keys(this.formulas).reduce((carry, key) => ({...carry, [key]: 0}), {ORE: 0});
        this.usage = {...this.leftovers};
    }

    calcFuelProduction(oreLimit) {
        this.makeOneFuel();
        let oreUsageForOneFuel = this.usage.ORE;

        let lowApprox = Math.round(oreLimit / oreUsageForOneFuel);
        let highApprox = lowApprox << 1;
        let result;

        do {
            this.debug && console.log(`Range: [${lowApprox} ... ${highApprox}]`);
            let approx = Math.round((lowApprox + highApprox) / 2);
            this.reset();

            this.debug && console.log(`Trying ${approx}`);
            this.make('FUEL', approx);
            if (this.usage.ORE > oreLimit) {
                this.debug && console.log(`Limit exceeded`);
                highApprox = approx - 1;
            } else {
                this.debug && console.log(`Limit not reached`);
                result = approx;
                lowApprox = approx;
            }
        } while (highApprox > lowApprox);
        this.debug && console.log('Found:', result);

        return result;
    }

    makeOneFuel() {
        this.reset();
        this.make('FUEL', 1);
        this.debug > 1 && (console.log('Usage:', this.usage), console.log('Leftovers:', this.leftovers));
    }

    make(chemical, amount, level = 0) {
        const dd = this.debug > 1 ? (...args) => {
            process.stdout.write(' '.repeat(level * 4));
            console.log(...args);
        } : () => {};
        dd('MAKE', chemical, amount);
        level++;
        const amountToProduce = (amount - this.leftovers[chemical]);
        if (amountToProduce <= 0) {
            return;
        }
        const {units, requires} = chemical === 'ORE' ? {units: amount, requires: []} : this.formulas[chemical];
        const mult = Math.ceil(amountToProduce / units);
        const amountMade = units * mult;
        for (let r of requires) {
            let rAmount = r.units * mult;
            this.make(r.name, rAmount, level);
            this.leftovers[r.name] -= rAmount;
            this.usage[r.name] += rAmount;
            dd('<= ' + r.name + ' -' + rAmount);
        }
        this.leftovers[chemical] += amountMade;
        dd('=> ' + chemical + ' +' + amountMade);
    }

}

module.exports = {Nanofactory, parseInput};
