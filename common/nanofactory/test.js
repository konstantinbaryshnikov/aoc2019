const {Nanofactory, parseInput} = require('.');
const {expectEq, expectEqs} = require('../tests');

function testInputParser() {
    const input = `9 ORE => 2 A
        8 ORE => 3 B
        7 ORE => 5 C
        3 A, 4 B => 1 AB
        5 B, 7 C => 1 BC
        4 C, 1 A => 1 CA
        2 AB, 3 BC, 4 CA => 1 FUEL`;
    const result = parseInput(input);

    const expected = {
        A:    {units: 2, requires: [{name: 'ORE', units: 9}]},
        B:    {units: 3, requires: [{name: 'ORE', units: 8}]},
        C:    {units: 5, requires: [{name: 'ORE', units: 7}]},
        AB:   {units: 1, requires: [{name: 'A', units: 3}, {name: 'B', units: 4}]},
        BC:   {units: 1, requires: [{name: 'B', units: 5}, {name: 'C', units: 7}]},
        CA:   {units: 1, requires: [{name: 'C', units: 4}, {name: 'A', units: 1}]},
        FUEL: {units: 1, requires: [{name: 'AB', units: 2}, {name: 'BC', units: 3}, {name: 'CA', units: 4}]},
    };

    Object.keys(result).forEach(key =>
        expectEqs(expected[key], result[key], `Parser failed for ${key}`)
    );
}

const testCases = [
    [
       `9 ORE => 2 A
        8 ORE => 3 B
        7 ORE => 5 C
        3 A, 4 B => 1 AB
        5 B, 7 C => 1 BC
        4 C, 1 A => 1 CA
        2 AB, 3 BC, 4 CA => 1 FUEL`,
        165
    ],
    [
       `157 ORE => 5 NZVS
        165 ORE => 6 DCFZ
        44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
        12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
        179 ORE => 7 PSHF
        177 ORE => 5 HKGWZ
        7 DCFZ, 7 PSHF => 2 XJWVT
        165 ORE => 2 GPVTF
        3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT`,
        13312,
        82892753
    ],
    [
       `2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
        17 NVRVD, 3 JNWZP => 8 VPVL
        53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
        22 VJHF, 37 MNCFX => 5 FWMGM
        139 ORE => 4 NVRVD
        144 ORE => 7 JNWZP
        5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
        5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
        145 ORE => 6 MNCFX
        1 NVRVD => 8 CXFTF
        1 VJHF, 6 MNCFX => 4 RFSQX
        176 ORE => 6 VJHF`,
        180697,
        5586022
    ],
    [
       `171 ORE => 8 CNZTR
        7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
        114 ORE => 4 BHXH
        14 VRPVC => 6 BMBT
        6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
        6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
        15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
        13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
        5 BMBT => 4 WPTQ
        189 ORE => 9 KTJDG
        1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
        12 VRPVC, 27 CNZTR => 2 XDBXC
        15 KTJDG, 12 BHXH => 5 XCVML
        3 BHXH, 2 VRPVC => 7 MZWV
        121 ORE => 7 VRPVC
        7 XCVML => 6 RJRHP
        5 BHXH, 4 VRPVC => 5 LTCX`,
        2210736,
        460664
    ],
];

function testMakeOneFuel() {
    testCases.forEach(([input, expectedORE]) => {
        const factory = Nanofactory.fromInput(input);
        // factory.debug = 2;
        factory.makeOneFuel();
        expectEq(expectedORE, factory.usage.ORE, 'testMakeOneFuel');
    });
}

function testOreLimit() {
    testCases.filter(c => c[2]).forEach(([input, _, expectedMaxFuel]) => {
        const factory = Nanofactory.fromInput(input);
        // factory.debug = 1;
        const fuel = factory.calcFuelProduction(1000000000000);
        expectEq(expectedMaxFuel, fuel, 'testOreLimit');
    });
}

testInputParser();
testMakeOneFuel();
testOreLimit();
