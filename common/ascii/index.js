const {machine} = require('../intcode');
const {program} = require('./program');

const ADJACENT_LOCATIONS_DELTAS = [[0, -1], [1, 0], [0, 1], [-1, 0]]; // clockwise order

class Ascii {

    loadMap() {
        const output = machine()(program.slice(0), []);
        this.setMap(output.map(c => String.fromCharCode(c)).join('').trim().split('\n'));
        return this.map;
    }

    go() {
        this.initStartPos();
        this.initIntersections();
        let commands = [];
        while (true) {
            if (this.stepInCurrentDirection()) {
                commands.push('X');
            } else {
                const rotation = this.rotate();
                if (!rotation) {
                    break;
                }
                commands.push(rotation);
            }
        }
        commands = this.compress(commands);
        const toAscii = s => (s.trim() + '\n').split('').map(c => c.charCodeAt(0));
        const inputs = [[], toAscii(commands.main), ...commands.sub.map(c => toAscii(c)), ['n'.charCodeAt(0), 10]];
        const prg = [2].concat(program.slice(1));
        const repl = machine().repl(prg);
        let output;
        inputs.forEach(input => {
            output = repl(input);
        });
        return output[output.length - 1];
    }

    compress(commands) {
        let xCount = 0;
        let compressed = [];
        const pushX = () => {
            if (xCount) {
                compressed.push(''+xCount);
                xCount = 0;
            }
        };
        for (let i = 0; i < commands.length; ++i) {
            if (commands[i] === 'X') {
                xCount++;
            } else {
                pushX();
                compressed.push(commands[i]);
            }
        }
        pushX();

        const compStr = compressed.join(',');
        const match = (compStr + ',').match(/^(.{1,22})\1*(.{1,22})(?:\1|\2)*(.{1,22})(?:\1|\2|\3)*$/);
        const names = ['A', 'B', 'C'];
        const cmds = match.slice(1, 4).map((s, i) => ({name: names[i], cmd: s.replace(/,$/, '')}));
        let mainCmd = compStr;
        let subcommands = [];
        for (const {name, cmd} of cmds) {
            mainCmd = mainCmd.split(cmd).join(name);
            subcommands.push(cmd);
        }
        return {main: mainCmd, sub: subcommands};
    }

    setMap(map) {
        this.map = map.map(line => line.split(''));
        this.width = map[0].length;
        this.height = map.length;
    }

    findIntersections() {
        const intersections = [];
        for (let y = 1; y < this.height - 2; ++y) {
            for (let x = 1; x < this.width - 2; x++) {
                if (this.get(x, y) === '#' && ADJACENT_LOCATIONS_DELTAS.every(([dx, dy]) => this.get(x + dx, y + dy) === '#')) {
                    intersections.push([x, y]);
                }
            }
        }
        return intersections;
    }

    initIntersections() {
        this.intersections = this.findIntersections().reduce((c, [x, y]) => ({...c, [this.toFlatCoord(x, y)]: true}), {});
    }

    isIntersection(x = this.x, y = this.y) {
        return this.intersections[this.toFlatCoord(x, y)];
    }

    toFlatCoord(x, y) {
        return y * this.width + x;
    }

    isValidMove(x, y) {
        return x >= 0 && x < this.width && y >= 0 && y < this.height && this.get(x, y) === '#';
    }

    stepInCurrentDirection() {
        let x = this.x + ADJACENT_LOCATIONS_DELTAS[this.direction][0];
        let y = this.y + ADJACENT_LOCATIONS_DELTAS[this.direction][1];
        if (!this.isValidMove(x, y)) {
            return false;
        }
        this.x = x;
        this.y = y;
        if (!this.isIntersection()) {
            this.map[this.y][this.x] = 'X';
        }
        return true;
    }

    rotate() {
        const currentLocation = [this.x, this.y];
        const rotateDirectionTo = diff => (this.direction + diff + ADJACENT_LOCATIONS_DELTAS.length) % ADJACENT_LOCATIONS_DELTAS.length;
        const rotations = [
            {command: 'L', direction: rotateDirectionTo(-1)},
            {command: 'R', direction: rotateDirectionTo(+1)},
        ];
        for (let {command, direction} of rotations) {
            const x = this.x + ADJACENT_LOCATIONS_DELTAS[direction][0];
            const y = this.y + ADJACENT_LOCATIONS_DELTAS[direction][1];
            if (this.isValidMove(x, y)) {
                this.direction = direction;
                return command;
            }
        }
        return undefined;
    }

    initStartPos() {
        for (let y = 0; y < this.height - 1; ++y) {
            for (let x = 0; x < this.width - 1; x++) {
                if (this.get(x, y) === '^') {
                    this.x = x;
                    this.y = y;
                    this.direction = 0;
                    return;
                }
            }
        }
        throw new Error('Not found');
    }

    calcAlignmentSum(intersections) {
        return intersections.reduce((c, i) => c + i[0] * i[1], 0);
    }

    get(x, y) {
        return this.map[y][x];
    }

    toString() {
        return this.map.map(line => line.join('')).join('\n');
    }

}

module.exports = {
    Ascii,
};
