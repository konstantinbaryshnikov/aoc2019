const {Ascii} = require('.');
const {expectEq, expectEqs} = require('../tests');

const testMap = `..#..........
..#..........
#######...###
#.#...#...#.#
#############
..#...#...#..
..#####...^..
`.split('\n');

function testIntersections() {
    const ascii = new Ascii();
    ascii.setMap(testMap);
    const expectedIntersections = [
        [2, 2],
        [2, 4],
        [6, 4],
        [10, 4],
    ];
    const expectedAlignentSum = 76;
    const intersections = ascii.findIntersections();
    const alignmentSum = ascii.calcAlignmentSum(intersections);
    intersections.sort((a, b) => a[0] - b[0] || a[0] - b[1]);
    expectEqs(expectedIntersections, intersections, 'intersections');
    expectEq(expectedAlignentSum, alignmentSum, 'alignment sum');
}

testIntersections();
