function permute(input) {
    const result = [];

    (function _permute(array, memo = []) {
        if (!array.length) {
            return result.push(memo);
        }

        for (let i = 0; i < array.length; ++i) {
            const clone = [...array];
            const nextMemo = memo.concat(clone.splice(i, 1));
            _permute(clone, nextMemo);
        }
    })(input);

    return result;
}

module.exports = {permute};
