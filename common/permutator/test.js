const {permute} = require('.');
const {expectEqs} = require('../tests');

function testPermutator() {
    const testCases = [
        [[0, 1], [[0, 1],
                   [1, 0]]
        ],
        [[0, 1, 2], [[ 0, 1, 2 ],
                     [ 0, 2, 1 ],
                     [ 1, 0, 2 ],
                     [ 1, 2, 0 ],
                     [ 2, 0, 1 ],
                     [ 2, 1, 0 ]]
        ],
    ];
    testCases.forEach(([input, expected]) => {
        const result = permute(input);
        expectEqs(expected, result, 'permute');
    });
}

testPermutator();
