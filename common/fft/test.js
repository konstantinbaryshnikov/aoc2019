const {pattern, parse, fft, mfft, mfftPartial} = require('.');
const {expectEq} = require('../tests');

function testPattern() {
    const testCases = [
        [1, 0, 1], [1, 1, 0], [1, 2, -1], [1, 3, 0], [1, 4, 1], [1, 5, 0], [1, 6, -1], [1, 7, 0], [1, 8, 1], [1, 9, 0],
        [2, 0, 0], [2, 1, 1], [2, 2, 1], [2, 3, 0], [2, 6, -1], [2, 7, 0],
        [3, 0, 0], [3, 1, 0], [3, 2, 1], [3, 10, -1], [3, 11, 0],
    ];
    testCases.forEach(([rep, x, exp]) => expectEq(exp, pattern(x, rep), `fft pattern x=${x} rep=${rep}`));
}

function testFft() {
    const testCases = [
        ['12345678', '48226158'],
        ['48226158', '34040438'],
        ['03415518', '01029498'],
    ];
    testCases.forEach(([input, exp]) => expectEq(exp, fft(parse(input)).join(''), `fft input=${input}`));
}

function testMfft() {
    const testCases = [
        ['80871224585914546619083218645595', '24176176', 100, 8],
        ['19617804207202209144916044189917', '73745418', 100, 8],
        ['69317163492948606335995924319873', '52432133', 100, 8],
    ];
    testCases.forEach(([input, exp, phases, len]) => expectEq(exp, mfft(parse(input), phases).slice(0, len).join(''), `mfft input=${input} phases=${phases} len=${len}`));
}

function testMfftPartial() {
    const testCases = [
        ['03036732577212944063491565474664', 10000, 100, 7, 8, '84462026'],
        ['02935109699940807407585447034323', 10000, 100, 7, 8, '78725270'],
        ['03081770884921959731165446850517', 10000, 100, 7, 8, '53553731'],
    ];
    testCases.forEach(([input, repeat, phases, offsetLen, len, exp]) => {
        const data = parse(input.repeat(repeat));
        const offset = input.substr(0, offsetLen) | 0;
        const result = mfftPartial(data, phases, offset).slice(offset, offset + len).join('');
        expectEq(exp, result);
    });
}

testPattern();
testFft();
testMfft();
testMfftPartial();
