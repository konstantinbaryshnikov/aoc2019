const basePattern = [0, 1, 0, -1];

const parse = s => s.trim().split('').map(x => x | 0);

const pattern = (x, rep) => basePattern[Math.floor((x + 1) / rep) % basePattern.length];

const lastDigit = n => Math.abs(n) % 10;
const rangeCache = new Map();
const range = length => rangeCache.has(length) ? rangeCache.get(length) : rangeCache.set(length, Array.from({length}).map((_, i) => i)).get(length);

const fft = input => 
    (r =>
        r.map(rep => 
            lastDigit(r.reduce((carry, x) =>
                carry + input[x] * pattern(x, rep + 1), 0)))
    )(range(input.length));

const mfft = (input, phases) => range(phases).reduce(carry => fft(carry), input);

const fftPartial = (input, offset) => {
    for (let i = input.length - 2; i >= offset; i--) {
        input[i] = (input[i] + input[i + 1]) % 10;
    }
    return input;
};

const mfftPartial = (input, phases, offset) => {
    console.assert(offset > input.length / 2, 'This only works for the second half');
    return range(phases).reduce(input => fftPartial(input, offset), input);
};

module.exports = {
    parse,
    pattern,
    fft,
    mfft,
    mfftPartial,
};
