const {program} = require('./program');
const {machine} = require('../intcode');

const WIDTH = 200;
const HEIGHT = 200;

class PaintBot {

    constructor(width = WIDTH, height = HEIGHT) {
        this.repl = machine().repl(program);
        this.map = Array.from({length: height}).map(_ => Array.from({length: width}).map(_ => ({color: 0, visited: 0})));
        this.x = (width >> 1) - 1;
        this.y = (height >> 1) - 1;
        this.visited = 0;
        this.direction = 0;

        const up = () => this.y--;
        const down = () => this.y++;
        const left = () => this.x--;
        const right = () => this.x++;

        this.move = () => [up, right, down, left][this.direction]();
    }

    step() {
        const output = this.repl([this.get().color]);
        if (output.length == 2) {
            const [color, direction] = output;
            this.paint(color);
            this.moveTo(direction);
        } else if (output.length && !output.halted) {
            throw new Error('WTF');
        }
        return output.halted;
    }

    moveTo(direction) {
        this.direction = (this.direction + 4 + (direction ? 1 : -1)) % 4;
        this.move();
    }

    get(x = this.x, y = this.y) {
        if (!this.map[y]) throw new Error("Y out of bounds: " + y);
        if (!this.map[y][x]) throw new Error("X out of bounds: " + x);
        return this.map[y][x];
    }

    paint(color, x = this.x, y = this.y) {
        this.setColor(color, x, y);
        if (!this.map[y][x].visited) {
            this.visited++;
        }
        this.map[y][x].visited++;
    }

    setColor(color, x = this.x, y = this.y) {
        if (!this.map[y]) throw new Error("Y out of bounds: " + y);
        if (!this.map[y][x]) throw new Error("X out of bounds: " + x);
        this.map[y][x].color = color;
    }

    toString() {
        return this.map.map(row => row.map(pos => pos.color ? '#' : '.').join('')).filter(s => ~s.indexOf('#')).join("\n");
    }

}

module.exports = {PaintBot};
