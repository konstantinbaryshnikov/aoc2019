const {parseInput} = require('./inputParser');
const {buildGraph, calcCrc, revPath, distance} = require('.');
const {expectEq, expectEqs} = require('../tests');

function testOrbitCrc() {
    const input = parseInput(`
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    COM)B
    D)I
    E)J
    J)K
    K)L`);

    const crc = calcCrc(input);
    expectEq(42, crc, 'orbit crc');
}

function testOrbitDistance() {
    const input = parseInput(`
    COM)B
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    D)I
    E)J
    J)K
    K)L
    K)YOU
    I)SAN`);
    const {rev, entrypoint} = buildGraph(input);
    const testCases = [
        ['L', [ 'L', 'K', 'J', 'E', 'D', 'C', 'B', 'COM' ]],
        ['F', [ 'F', 'E', 'D', 'C', 'B', 'COM' ]],
        ['B', [ 'B', 'COM' ]],
    
    ];
    testCases.forEach(([ptr, expected]) => {
        const path = revPath(rev, ptr);
        expectEqs(expected, path, 'path: ' + ptr);
    });

    expectEq(4, distance(input, 'YOU', 'SAN'), 'distance 1');
    expectEq(6, distance(input, 'YOU', 'H'), 'distance 2');
}

testOrbitCrc();
testOrbitDistance();
