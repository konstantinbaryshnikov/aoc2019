function parseInput(mapInput) {
    return mapInput.trim().split(/\s+/).map(s => s.split(')'));
}

module.exports = {
    parseInput
};
