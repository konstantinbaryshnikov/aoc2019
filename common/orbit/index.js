function buildGraph(map) {
    const graph = {};
    const rev = {};
    map.forEach(([a, b]) => {
        if (!graph[a]) {
            graph[a] = [];
        }
        graph[a].push(b);
        if (!rev[a]) {
            rev[a] = [];
        }
        if (!rev[b]) {
            rev[b] = [];
        }
        rev[b].push(a);
    });
    const roots = Object.keys(rev).filter(k => !rev[k].length);
    if (roots.length !== 1) {
        throw new Error("WTF");
    }

    return {
        graph,
        rev,
        root: roots[0]
    };
}

function calcCrc(map) {
    const {graph, root} = buildGraph(map);
    const orbits = calcOrbits(graph, root);
    return orbits.reduce((a, b) => a + b - 1, orbits.length);
}

function distance(map, a, b) {
    const {rev} = buildGraph(map);
    const pathA = revPath(rev, a).slice(1).reverse();
    const pathB = revPath(rev, b).slice(1).reverse();
    while (pathA[0] === pathB[0]) {
        pathA.shift();
        pathB.shift();
    }
    return pathA.length + pathB.length;
}

function revPath(rev, entrypoint) {
    const parents = rev[entrypoint];
    switch (parents.length) {
        case 0:
            return [entrypoint];
        case 1:
            return [entrypoint].concat(revPath(rev, parents[0]));
        default:
            throw new Error('wtf');
    }
}

function calcOrbits(graph, entrypoint, level = 0) {
    const children = graph[entrypoint] || [];
    const result = [];
    children.forEach(child => result.push(...calcOrbits(graph, child, level + 1)));
    if (level) result.push(level);
    return result;
}

module.exports = {
    buildGraph,
    calcCrc,
    revPath,
    distance,
};
