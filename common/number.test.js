const {gcd, lcm} = require('./number');
const {expectEq} = require('./tests');

function testGcd() {
    expectEq(21, gcd(1071, 462));
    expectEq(3, gcd(3 * 3, 3 * 11, 3 * 13));
}

function testLcm() {
    expectEq(4686774924, lcm(2028, 5898, 4702));
}

testGcd();
testLcm();
