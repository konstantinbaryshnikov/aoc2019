const {AsteroidMap} = require('.');
const {expectEq, expectEqs} = require('../tests');

function testAsteroidMapIterator() {
    const map = new AsteroidMap(`
        .##
        ##.
    `);
    expectEq(3, map.width);
    expectEq(2, map.height);
    const expected = [
        { x: 1, y: 0, },
        { x: 2, y: 0, },
        { x: 0, y: 1, },
        { x: 1, y: 1, }
    ];
    const asteroids = Array.from(map.asteroids());
    expectEqs(expected, asteroids);
}

function testAsteroidMapVisibility() {
    const testCases = [
        [
           `#..
            .#.
            ..#`,
            [
                {x: 0, y: 0, expected: 1},
                {x: 1, y: 0, expected: 3},
                {x: 1, y: 1, expected: 2},
                {x: 2, y: 2, expected: 1},
            ],
            {x: 1, y: 1, visible: 2}
        ],
        [
           `.#..#
            .....
            #####
            ....#
            ...##`,
            [
                {x: 1, y: 0, expected: 7},
                {x: 4, y: 0, expected: 7},
                {x: 0, y: 2, expected: 6},
                {x: 2, y: 2, expected: 7},
                {x: 4, y: 2, expected: 5},
                {x: 4, y: 3, expected: 7},
                {x: 3, y: 4, expected: 8},
            ],
            {x: 3, y: 4, visible: 8}
        ],
    ];
    testCases.forEach(([input, points, bestExpected]) => {
        const map = new AsteroidMap(input);
        points.forEach(point => {
            const visible = map.countVisibleFrom(point);
            expectEq(point.expected, visible, `visible from ${point.x},${point.y} on ${input}`);
        });
        const best = map.findBestAsteroid();
        expectEqs(bestExpected, best, `best on ${input}`);
    });
}

function testAsteroidMapVaporize() {
    const input = `
        .#....#####...#..
        ##...##.#####..##
        ##...#...#.#####.
        ..#.....#...###..
        ..#.#.....#....##
    `;
    const map = new AsteroidMap(input);
    const vaporized = map.vaporize({x: 8, y: 3});
    expectEq(Array.from(map.asteroids()).length - 1, vaporized.length);
    expectEqs({x: 8, y: 1}, vaporized[0]);
    expectEqs({x: 9, y: 0}, vaporized[1]);
    expectEqs({x: 9, y: 1}, vaporized[2]);
    expectEqs({x: 14, y: 3}, vaporized[vaporized.length - 1]);
}

testAsteroidMapIterator();
testAsteroidMapVisibility();
testAsteroidMapVaporize();
