function polar(x, y) {
    const r2 = x**2 + y**2;
    const p = (Math.atan2(x, y) * 180000 / Math.PI) | 0;
    return {r2, p};
}

function asteroid(x, y) {
    return {x, y};
}

class AsteroidMap {

    constructor(input) {
        this.map = input.trim().split(/\s+/).map((row, y) => row.split('').map((c, x) => c === '#' ? asteroid(x, y) : null));
        this.width = this.map[0].length;
        this.height = this.map.length;
    }

    *asteroids() {
        for (let y = 0; y < this.height; ++y) {
            for (let x = 0; x < this.width; ++x) {
                if (this.map[y][x]) {
                    yield this.map[y][x];
                }
            }
        }
    }

    countVisibleFrom(point) {
        return Array.from(this.asteroids()).reduce((set, asteroid) => {
            const {r2, p} = polar(asteroid.x - point.x, asteroid.y - point.y);
            if (r2 > 0) {
                set.add(p);
            }
            return set;
        }, new Set()).size;
    }

    vaporize(point) {
        const polarGroups = Object.values(Array.from(this.asteroids()).reduce((map, asteroid) => {
            const {r2, p} = polar(asteroid.x - point.x, asteroid.y - point.y);
            if (r2 > 0) {
                if (!map[p]) {
                    map[p] = {p, asteroids: []};
                }
                map[p].asteroids.push({...asteroid, r2, p});
            }
            return map;
        }, {}));
        polarGroups.sort((a, b) => b.p - a.p);
        polarGroups.forEach(g => g.asteroids.sort((a, b) => b.r2 - a.r2));
        const vaporized = [];
        while (polarGroups.length) {
            for (let gi = 0; gi < polarGroups.length; ) {
                let {x, y} = polarGroups[gi].asteroids.pop();
                vaporized.push({x, y});
                if (polarGroups[gi].asteroids.length) {
                    ++gi;
                } else {
                    polarGroups.splice(gi, 1);
                }
            }
        }
        return vaporized;
    }

    findBestAsteroid() {
        let maxVisible = -1, bestAsteroid;
        for (let asteroid of this.asteroids()) {
            let visible = this.countVisibleFrom(asteroid);
            if (visible > maxVisible) {
                maxVisible = visible;
                bestAsteroid = asteroid;
            }
        }
        return bestAsteroid && {...bestAsteroid, visible: maxVisible};
    }

}

module.exports = {AsteroidMap}
