const {machine} = require('../intcode');

function amplifier(program, debug = false, machinesCount = 5) {
    const repls = Array.from({length: machinesCount}).map(() => machine(debug).repl([...program]));
    return function(phases, signal) {
        if (phases.length !== machinesCount) throw new Error('Invalid phases arg');
        for (let i = 0; i < machinesCount; ++i) {
            let output = repls[i]([phases[i], signal]);
            signal = output[0];
        }
        return signal;
    }
}

function feedbackLoop(program, debug = false, machinesCount = 5) {
    const repls = Array.from({length: machinesCount}).map(() => machine(debug).repl([...program]));
    return function(phases, signal) {
        if (phases.length !== machinesCount) throw new Error('Invalid phases arg');
        let initial = true;
        let result;
        let halted;
        while (true) {
            for (let i = 0; i < repls.length; ++i) {
                let input = [signal];
                if (initial) {
                    input.unshift(phases[i]);
                }
                debug && console.log(`> Amp ${i}, input ${input}`);
                output = repls[i](input);
                debug && console.log(`< Amp ${i}, output ${output}`);
                if (output.length == 0) {
                    return result;
                } else {
                    signal = output[0];
                }
            }
            initial = false;
            result = signal;
        }
        return result;
    }
}


module.exports = {
    amplifier, 
    feedbackLoop,
};
