const {machine} = require('../intcode');

class Arcade {

    constructor(inProgram, w, h, quarters) {
        let program = [...inProgram];
        this.width = w;
        this.height = h;
        this.screen = Array.from({length: h}).map(() => Array.from({length: w}).map(() => 0));
        if (quarters) {
            program[0] = quarters;
        }
        this.eval = machine().repl(program);
        this.lastTile = [];
        this.score = 0;
    }

    step(input) {
        const output = this.eval([...input]);
        if (output.length % 3) throw new Error("Invalid output");
        for (let i = 0; i < output.length; i += 3) {
            this.display(...output.slice(i, i + 3));
        }

        return output.halted;
    }

    display(x, y, id) {
        if (x === -1 && y === 0) {
            this.score = id;
            return;
        }
        if (x < 0 || x >= this.width) throw new Error(`x=${x} is out of range`);
        if (y < 0 || y >= this.height) throw new Error(`y=${y} is out of range`);
        this.screen[y][x] = id;
        this.lastTile[id] = [x, y];
    }

    countOf(tileId) {
        return this.screen.reduce((c, line) => c + line.filter(id => id === tileId).length, 0);
    }

    toString() {
        const score = '[Score: ' + this.score + ']';
        const h = '+' + score + '-'.repeat(this.width - score.length) + '+';
        return [h].
            concat(this.screen.map(line => '|' + line.map(id => ' @#-o'.charAt(id)).join('') + '|')).
            concat(h).
            join("\n") + "\n";
    }

}

module.exports = {Arcade};
