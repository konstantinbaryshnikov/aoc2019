const {toDigits} = require('../number');

function machine(debug = false) {
    const DST_OUTPUT = 0;
    const DST_POS = -1;
    const RET_HALTED = -1;
    const RET_NEED_INPUT = -2;

    const decodeModes = modes => toDigits(1000 + modes).slice(1).reverse();

    const decodeCommand = command => ({
        opcodeNumber: command % 100,
        modes: decodeModes(command / 100 | 0),
    });

    const evalOpcode = (tape, input, output) => {
        const pos = tape.pos;
        const opcodes = {
            1: {
                id: 'add',
                eval: (x, y) => x + y,
                dst: 3,
                len: 4,
            },
            2: {
                id: 'mult',
                eval: (x, y) => x * y,
                dst: 3,
                len: 4,
            },
            3: {
                id: 'input',
                eval: () => {
                    if (!input.length) {
                        throw new Error('EOF');
                    }
                    return input.shift();
                },
                dst: 1,
                len: 2,
            },
            4: {
                id: 'output',
                eval: x => x,
                dst: DST_OUTPUT,
                len: 2,
            },
            5: {
                id: 'if',
                eval: (x, y) => x ? y : undefined,
                dst: DST_POS,
                len: 3,
            },
            6: {
                id: 'unless',
                eval: (x, y) => x ? undefined : y,
                dst: DST_POS,
                len: 3,
            },
            7: {
                id: 'lt',
                eval: (x, y) => x < y ? 1 : 0,
                dst: 3,
                len: 4,
            },
            8: {
                id: 'eq',
                eval: (x, y) => x == y ? 1 : 0,
                dst: 3,
                len: 4,
            },
            9: {
                id: 'setrelbase',
                eval: x => {
                    tape.relbase += x;
                    return undefined;
                },
                len: 2,
            },
            99: {
                id: 'halt',
                eval: () => {
                    output.halted = true;
                    return RET_HALTED;
                },
                dst: DST_POS,
                len: 1
            },
        };

        const {opcodeNumber, modes} = decodeCommand(tape[pos]);
        const opcode = opcodes[opcodeNumber];
        if (!opcode) {
            throw new Error(`Invalid opcode: ${opcodeNumber}`);
        }
        const argc = opcode.eval.length;
        const argv = Array.from({length: argc}).map((_, argn) => (value => [x => tape[x] || 0, x => x, x => tape[x + tape.relbase] || 0][modes[argn]](value))(tape[pos + argn + 1]));
        debug && console.info(`>>> Opcode ${opcodeNumber} ${opcode.id}, modes: ${modes}, params: ` + tape.slice(pos + 1, pos + opcode.len).join(',') + `, argv: ${argv}, pos: ${pos}, relbase: ${tape.relbase}`);
        let result;
        try {
            result = opcode.eval(...argv);
        } catch (e) {
            if (e.message === 'EOF') {
                debug && console.info(`<<< NEED_INPUT`);
                return RET_NEED_INPUT;
            }
            throw e;
        }
        if (result !== undefined) {
            switch (opcode.dst) {
                case DST_POS:
                    debug && console.info(`<<< DST_POS ${result}`);
                    return result;
                case DST_OUTPUT: 
                    debug && console.info(`<<< OUTPUT ${result}`);
                    output.push(result);
                    break;
                default:
                    let dst = tape[pos + opcode.dst];
                    if (modes[opcode.dst - 1] === 2) {
                        dst += tape.relbase;
                    }
                    tape[dst] = result;
                    debug && console.info(`<<< WRITE [${dst}] <- ${result}`);
            }
        }
        const nextPos = pos + opcode.len;
        debug && console.info(`<<< NEXT_POS ${nextPos}`);
        return nextPos;
    }

    function coredump(tape, input, output, error) {
        const pos = tape.pos;
        console.error("INTCODE MACHINE CRASHED: " + error.message);
        console.debug("Tape length   : " + tape.length);
        console.debug("Tape position : " + pos);
        console.debug("Tape contents (starting at " + Math.max(0, pos - 12) + '): ');
        console.debug("              : " + tape.slice(Math.max(0, pos - 12), Math.max(0, pos)).join(',') + ' ,>' + tape[pos] + '<, ' + tape.slice(pos + 1, pos + 12).join(','));
        console.debug("Input (unread): " + input.join(','));
        console.debug("Output        : " + output.join(','));
    }

    function evalCurrentOpcode(tape, input, output) {
        try {
            return evalOpcode(tape, input, output);
        } catch (e) {
            coredump(tape, input, output, e);
            throw e;
        }
    }

    function machineLoop(tape, input = []) {
        return machineLoop.repl(tape)(input);
    };

    machineLoop.repl = tape => {
        tape.pos = 0;
        tape.relbase = 0;
        return input => {
            const output = [];
            let nextPos = tape.pos;
            while (true) {
                if (nextPos === RET_HALTED) {
                    output.halted = true;
                }
                if (nextPos < 0) {
                    return output;
                }
                tape.pos = nextPos;
                nextPos = evalCurrentOpcode(tape, input, output);
            }
        };
    };

    return machineLoop;
};

module.exports = {machine};
