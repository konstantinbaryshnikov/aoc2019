const {lcm} = require('../number');

function parseInput(input) {
    return input.trim().split(/\s*\n\s*/)
        .map(line => line.match(/<x=\s*(-?\d+), y=\s*(-?\d+), z=\s*(-?\d+)>/).slice(1, 4).map(v => v | 0))
        .map(position => ({position, velocity: [0, 0, 0]}));
}

class NBodySimulator {

    static constructFromInput(input, coords = [0, 1, 2]) {
        return new this(parseInput(input), coords);
    }

    static findCycleFromInput(input, coords = [0, 1, 2]) {
        return this.findCycle(parseInput(input), coords);
    }

    static findCycle(bodies, coords = [0, 1, 2]) {
        const cycles = coords.map(i => {
            const simX = new this(bodies, [i]);
            const orig = simX.getBodiesAtAxis(i);
            const isOrig = axis => axis.positions.every((v, i) => v === orig.positions[i]) && axis.velocities.every((v, i) => v === orig.velocities[i]);
            let step = 0, axis;
            do {
                simX.step();
                ++step;
                axis = simX.getBodiesAtAxis(i);
            } while (!isOrig(axis));
            return step;
        });
        return lcm(...cycles);
    }

    constructor(bodies, coords = [0, 1, 2]) {
        this.bodies = bodies;
        this.coords = coords;
        this.pairs = this.bodies.flatMap((a, i) => this.bodies.slice(i + 1).map(b => [a, b]));
        this.calcEnergy();
    }

    step(times = 1) {
        while (times-- > 0) {
            this.forEachPair((a, b) => this.applyGravity(a, b));
            this.forEachBody(body => this.applyVelocity(body));
        }
        this.calcEnergy();
    }

    getBodiesAtAxis(i) {
        return this.bodies.reduce((carry, body) => ({
            positions: carry.positions.concat(body.position[i]),
            velocities: carry.velocities.concat(body.velocity[i])
        }), {positions: [], velocities: []});
    }

    applyGravity(a, b) {
        this.forEachCoordinate(i => {
            if (a.position[i] < b.position[i]) {
                a.velocity[i]++;
                b.velocity[i]--;
            } else if (a.position[i] > b.position[i]) {
                a.velocity[i]--;
                b.velocity[i]++;
            }
        });
    }

    applyVelocity(body) {
        this.forEachCoordinate(i => {
            body.position[i] += body.velocity[i];
        });
    }

    calcEnergy() {
        this.energies = {pot: [], kin: [], total: []};
        this.forEachBody(body => {
            let pot, kin;
            this.energies.pot.push(pot = body.position.reduce((c, v) => Math.abs(c) + Math.abs(v)));
            this.energies.kin.push(kin = body.velocity.reduce((c, v) => Math.abs(c) + Math.abs(v)));
            this.energies.total.push(pot * kin);
        });
        this.energy = {
            pot: this.energies.pot.reduce((c, v) => c + v),
            kin: this.energies.kin.reduce((c, v) => c + v),
            total: this.energies.total.reduce((c, v) => c + v),
        };
    }

    forEachPair(fn) {
        this.pairs.forEach(pair => fn(...pair));
    }

    forEachBody(fn) {
        this.bodies.forEach(body => fn(body));
    }

    forEachCoordinate(fn) {
        this.coords.forEach(coord => fn(coord));
    }

}

module.exports = {NBodySimulator};
