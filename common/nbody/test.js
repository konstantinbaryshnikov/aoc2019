const {expectEq, expectEqs} = require('../tests');
const {NBodySimulator} = require('.');

function parseExpectations(input) {
    const stepRegExp = /pos=<x=\s*(-?\d+), y=\s*(-?\d+), z=\s*(-?\d+)>, vel=<x=\s*(-?\d+), y=\s*(-?\d+), z=\s*(-?\d+)>/g;
    return input.split('After ').filter(step => step.trim().length).map(step => {
        let match;
        const bodies = [];
        while (match = stepRegExp.exec(step)) {
            match.shift();
            bodies.push({position: match.slice(0, 3).map(v => v | 0), velocity: match.slice(3, 6).map(v => v | 0)});
        }
        return bodies;
    });
}

function testStep() {
    const expectations = parseExpectations(`
    After 0 steps:
    pos=<x=-1, y=  0, z= 2>, vel=<x= 0, y= 0, z= 0>
    pos=<x= 2, y=-10, z=-7>, vel=<x= 0, y= 0, z= 0>
    pos=<x= 4, y= -8, z= 8>, vel=<x= 0, y= 0, z= 0>
    pos=<x= 3, y=  5, z=-1>, vel=<x= 0, y= 0, z= 0>

    After 1 step:
    pos=<x= 2, y=-1, z= 1>, vel=<x= 3, y=-1, z=-1>
    pos=<x= 3, y=-7, z=-4>, vel=<x= 1, y= 3, z= 3>
    pos=<x= 1, y=-7, z= 5>, vel=<x=-3, y= 1, z=-3>
    pos=<x= 2, y= 2, z= 0>, vel=<x=-1, y=-3, z= 1>

    After 2 steps:
    pos=<x= 5, y=-3, z=-1>, vel=<x= 3, y=-2, z=-2>
    pos=<x= 1, y=-2, z= 2>, vel=<x=-2, y= 5, z= 6>
    pos=<x= 1, y=-4, z=-1>, vel=<x= 0, y= 3, z=-6>
    pos=<x= 1, y=-4, z= 2>, vel=<x=-1, y=-6, z= 2>

    After 3 steps:
    pos=<x= 5, y=-6, z=-1>, vel=<x= 0, y=-3, z= 0>
    pos=<x= 0, y= 0, z= 6>, vel=<x=-1, y= 2, z= 4>
    pos=<x= 2, y= 1, z=-5>, vel=<x= 1, y= 5, z=-4>
    pos=<x= 1, y=-8, z= 2>, vel=<x= 0, y=-4, z= 0>

    After 4 steps:
    pos=<x= 2, y=-8, z= 0>, vel=<x=-3, y=-2, z= 1>
    pos=<x= 2, y= 1, z= 7>, vel=<x= 2, y= 1, z= 1>
    pos=<x= 2, y= 3, z=-6>, vel=<x= 0, y= 2, z=-1>
    pos=<x= 2, y=-9, z= 1>, vel=<x= 1, y=-1, z=-1>

    After 5 steps:
    pos=<x=-1, y=-9, z= 2>, vel=<x=-3, y=-1, z= 2>
    pos=<x= 4, y= 1, z= 5>, vel=<x= 2, y= 0, z=-2>
    pos=<x= 2, y= 2, z=-4>, vel=<x= 0, y=-1, z= 2>
    pos=<x= 3, y=-7, z=-1>, vel=<x= 1, y= 2, z=-2>

    After 6 steps:
    pos=<x=-1, y=-7, z= 3>, vel=<x= 0, y= 2, z= 1>
    pos=<x= 3, y= 0, z= 0>, vel=<x=-1, y=-1, z=-5>
    pos=<x= 3, y=-2, z= 1>, vel=<x= 1, y=-4, z= 5>
    pos=<x= 3, y=-4, z=-2>, vel=<x= 0, y= 3, z=-1>

    After 7 steps:
    pos=<x= 2, y=-2, z= 1>, vel=<x= 3, y= 5, z=-2>
    pos=<x= 1, y=-4, z=-4>, vel=<x=-2, y=-4, z=-4>
    pos=<x= 3, y=-7, z= 5>, vel=<x= 0, y=-5, z= 4>
    pos=<x= 2, y= 0, z= 0>, vel=<x=-1, y= 4, z= 2>

    After 8 steps:
    pos=<x= 5, y= 2, z=-2>, vel=<x= 3, y= 4, z=-3>
    pos=<x= 2, y=-7, z=-5>, vel=<x= 1, y=-3, z=-1>
    pos=<x= 0, y=-9, z= 6>, vel=<x=-3, y=-2, z= 1>
    pos=<x= 1, y= 1, z= 3>, vel=<x=-1, y= 1, z= 3>

    After 9 steps:
    pos=<x= 5, y= 3, z=-4>, vel=<x= 0, y= 1, z=-2>
    pos=<x= 2, y=-9, z=-3>, vel=<x= 0, y=-2, z= 2>
    pos=<x= 0, y=-8, z= 4>, vel=<x= 0, y= 1, z=-2>
    pos=<x= 1, y= 1, z= 5>, vel=<x= 0, y= 0, z= 2>

    After 10 steps:
    pos=<x= 2, y= 1, z=-3>, vel=<x=-3, y=-2, z= 1>
    pos=<x= 1, y=-8, z= 0>, vel=<x=-1, y= 1, z= 3>
    pos=<x= 3, y=-6, z= 1>, vel=<x= 3, y= 2, z=-3>
    pos=<x= 2, y= 0, z= 4>, vel=<x= 1, y=-1, z=-1>
    `);
    const input = `
    <x=-1, y=0, z=2>
    <x=2, y=-10, z=-7>
    <x=4, y=-8, z=8>
    <x=3, y=5, z=-1>
    `;
    const sim = NBodySimulator.constructFromInput(input);
    while (expectations.length) {
        expectEqs(expectations.shift(), sim.bodies);
        sim.step();
    }
}

function testEnergy() {
    const testCases = [
        [10, 179, `
        <x=-1, y=0, z=2>
        <x=2, y=-10, z=-7>
        <x=4, y=-8, z=8>
        <x=3, y=5, z=-1>
        `],
        [100, 1940, `
        <x=-8, y=-10, z=0>
        <x=5, y=5, z=10>
        <x=2, y=-7, z=3>
        <x=9, y=-8, z=-3>
        `],
    ];
    testCases.forEach(([steps, expected, input]) => {
        const sim = NBodySimulator.constructFromInput(input);
        sim.step(steps);
        expectEq(expected, sim.energy.total);
    });
}

function testCycle() {
    const input = `
    <x=-8, y=-10, z=0>
    <x=5, y=5, z=10>
    <x=2, y=-7, z=3>
    <x=9, y=-8, z=-3>
    `;

    const expectAxisSteps = [2028, 5898, 4702];
    let axisSteps = [];
    for (let c = 0; c < 3; c++) {
        const simX = NBodySimulator.constructFromInput(input, [c]);
        const orig = simX.getBodiesAtAxis(c);
        const isOrig = axis => axis.positions.every((v, i) => v === orig.positions[i]) && axis.velocities.every((v, i) => v === orig.velocities[i]);
        let step = 0, axis;
        do {
            simX.step();
            ++step;
            axis = simX.getBodiesAtAxis(c);
        } while (!isOrig(axis));
        expectEq(expectAxisSteps[c], step);
    }

    const expectedResult = 4686774924;
    expectEq(expectedResult, NBodySimulator.findCycleFromInput(input));
}

testStep();
testEnergy();
testCycle();
