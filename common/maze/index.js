const {machine} = require('../intcode');
const {program} = require('./program');

const EMPTY = '  ';
const WALL = '▓▓';
const DISCOVERED = '░░';
const TARGET = '🔥';

const MOVES = [[0, -1], [0, 1], [-1, 0], [1, 0]];

const DEFAULT_WIDTH = 50;
const DEFAULT_HEIGHT = 50;

class MazeWalker {

    constructor(width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT) {
        this.eval = machine().repl(program);
        this.width = width;
        this.height = height;
        this.x = this.width >> 1;
        this.y = this.height >> 1;
        this.map = Array.from({length: height}).map(() => Array.from({length: width}).map(() => ({})));
    }

    findGoal() {
        return this.discover(true);
    }

    discover(untilFound = false) {
        this.set({type: DISCOVERED});
        const queue = [];
        queue.push(this.coords());
        while (queue.length) {
            const location = queue.shift();
            this.goTo(location);
            if (location.x !== this.x || location.y !== this.y) {
                throw new Error('WTF');
            }
            const moves = MOVES.filter(([dx, dy]) => !this.get(this.x + dx, this.y + dy).type);
            for (let [dx, dy] of moves) {
                const status = this.move(dx, dy);
                if (status) {
                    const discoveredLocation = this.coords();
                    this.set({parent: {...location}, type: [DISCOVERED, TARGET][status - 1]});
                    if (untilFound && status === 2) {
                        return discoveredLocation;
                    } else if (status === 2) {
                        this.targetLocation = discoveredLocation;
                    }
                    queue.push(discoveredLocation);
                    this.moveBack(dx, dy);
                } else {
                    this.set({type: WALL}, this.x + dx, this.y + dy);
                }
            }
        }
    }

    fill() {
        this.x = this.targetLocation.x;
        this.y = this.targetLocation.y;
        let queue = [this.coords()], nextQueue;
        let steps = 0;
        while (true) {
            nextQueue = [];
            for (let location of queue) {
                const adjacentLocations = MOVES.filter(([dx, dy]) => this.get(location.x + dx, location.y + dy).type === DISCOVERED).
                    map(([dx, dy]) => ({x: location.x + dx, y: location.y + dy}));
                for (let adj of adjacentLocations) {
                    this.set({type: TARGET}, adj.x, adj.y);
                    nextQueue.push(adj);
                }
            }
            if (nextQueue.length) {
                queue = nextQueue;
                steps++;
                this.debug && this.debug(null, steps);
            } else {
                return steps;
            }
        }
    }

    move(...move) {
        const command = 1 + MOVES.findIndex(c => c.every((v, i) => v === move[i]));
        const output = this.eval([command]);
        if (!output.length) {
            throw new Error('WTF');
        }
        const status = output[0] | 0;
        if (!~[0, 1, 2].indexOf(status)) {
            throw new Error('WTF');
        }
        if (status) {
            const [dx, dy] = move;
            this.x += dx;
            this.y += dy;
        }
        return status;
    }

    moveBack(...move) {
        return this.move(...move.map(x => -x));
    }

    goTo(location) {
        for (let move of this.getMovesTo(location)) {
            const status = this.move(...move);
            if (status === 0) {
                throw new Error('WTF');
            }
        }
    }

    getMovesTo(target, src = this.coords()) {
        if (this.cmpPath(src, target)) {
            return [];
        }
        let tgtPath = this.getPathTo(target);
        let srcPath = this.getPathTo(src);
        let intersection;
        while (srcPath.length && tgtPath.length && this.cmpPath(tgtPath[0], srcPath[0])) {
            intersection = tgtPath[0];
            tgtPath.shift();
            srcPath.shift();
        }
        const path = srcPath.reverse().concat(intersection || []).concat(tgtPath);
        const moves = [];
        for (let dest of path) {
            if (!this.cmpPath(dest, src)) {
                moves.push([dest.x - src.x, dest.y - src.y]);
            }
            src = dest;
        }
        return moves;
    }

    getPathTo(location) {
        const path = [location];
        let parent;
        while (parent = this.getLoc(location).parent) {
            path.unshift(parent);
            location = parent;
        }
        return path;
    }

    cmpPath(p1, p2) {
        return p1.x === p2.x && p1.y === p2.y;
    }

    coords(x = this.x, y = this.y) {
        return {x, y};
    }

    get(x = this.x, y = this.y) {
        this.validateCoords(x, y);
        return this.map[y][x];
    }

    getLoc(location) {
        return this.get(location.x, location.y);
    }

    set(value, x = this.x, y = this.y) {
        this.validateCoords(x, y);
        Object.assign(this.map[y][x], value);
        this.debug && this.debug(this.toString());
    }

    validateCoords(x, y) {
        if (x < 0 || x >= this.width) {
            throw new Error(`x=${x} is out of bounds`);
        }
        if (y < 0 || y >= this.height) {
            throw new Error(`y=${y} is out of bounds`);
        }
    }

    toString() {
        const chr = (loc, x, y) => {
            const isCurrent = x === this.x && y === this.y;
            return [isCurrent ? "\x1b[7m" : '', loc.type || EMPTY, isCurrent ? "\x1b[0m" : ''].join('');
        };
        return this.map.map((line, y) => '' + y + ' '.repeat(5 - `${y}`.length) + '|' + line.map((loc, x) => chr(loc, x, y)).join('') + "|\n").join('');
    }

}

module.exports = {
    MazeWalker,
};
