const {machine} = require('../common/intcode');
const {program} = require('./input');

const tape = program(12, 2);
machine()(tape);
console.log(tape[0]);
