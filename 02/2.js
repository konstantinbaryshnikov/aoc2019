const {machine} = require('../common/intcode');
const {program} = require('./input');

const expected = 19690720;

function bruteforce() {
    const eval = machine();
    for (let x = 0; x <= 99; x++) {
        for (let y = 0; y <= 99; y++) {
            try {
                const tape = program(x, y);
                eval(tape);
                if (expected === tape[0]) {
                    return 100 * x + y;
                }
            } catch (e) {
                // who cares
            }
        }
    }
}

console.log(bruteforce());
