const {matchesConstraints} = require('./password');

const range = [172851, 675869];

let count = 0;

for (let i = range[0]; i <= range[1]; i++) {
    if (matchesConstraints(i)) {
        count++;
    }
}

console.log(count);
