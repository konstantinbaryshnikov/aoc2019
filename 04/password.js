const {toDigits} = require('../common/number');

function matchesDigitsRules(digits) {
    let doublesFound = 0;
    for (let i = 1; i < digits.length; i++) {
        if (digits[i] < digits[i - 1]) {
            return false;
        }
        if (digits[i] === digits[i - 1]) {
            doublesFound++;
        }
    }
    return doublesFound > 0;
}

function matchesDigitsRulesEx(digits) {
    let doublesFound = Array.from({length: 10}, () => 0);
    for (let i = 1; i < digits.length; i++) {
        if (digits[i] < digits[i - 1]) {
            return false;
        }
        if (digits[i] === digits[i - 1]) {
            doublesFound[digits[i]]++;
        }
    }
    return doublesFound.some(c => c === 1);
}

function matchesConstraints(x, min = 100000, max = 999999) {
    return x >= min
        && x <= max
        && matchesDigitsRules(toDigits(x));
}

function matchesConstraintsEx(x, min = 100000, max = 999999) {
    return x >= min
        && x <= max
        && matchesDigitsRulesEx(toDigits(x));
}

module.exports = {
    matchesConstraints,
    matchesConstraintsEx,
};
