const {matchesConstraints, matchesConstraintsEx} = require('./password');
const {expectIs} = require('../common/tests');

function testPasswordConstraints() {
    const testCases = [
        [111111, true],
        [223450, false],
        [123789, false],
    ];
    testCases.forEach(testCase => expectIs(testCase[1], matchesConstraints(testCase[0]), 'password constraints for ' + testCase[0]));
}

function testPasswordConstraintsEx() {
    const testCases = [
        [112233, true],
        [123444, false],
        [111122, true],
    ];
    testCases.forEach(testCase => expectIs(testCase[1], matchesConstraintsEx(testCase[0]), 'password constraints_ex for ' + testCase[0]));
}

testPasswordConstraints();
testPasswordConstraintsEx();
