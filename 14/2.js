const {Nanofactory} = require('../common/nanofactory');
const {input} = require('./input');

const factory = Nanofactory.fromInput(input);
const result = factory.calcFuelProduction(1000000000000);
console.log(result);
