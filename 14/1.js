const {Nanofactory} = require('../common/nanofactory');
const {input} = require('./input');

const factory = Nanofactory.fromInput(input);
factory.makeOneFuel();
console.log(factory.usage.ORE);
