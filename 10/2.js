const {AsteroidMap} = require('../common/asteroid_map');
const {input} = require('./input');

const map = new AsteroidMap(input);
const station = map.findBestAsteroid();
const vaporized = map.vaporize(station);
console.log(vaporized[199]);
