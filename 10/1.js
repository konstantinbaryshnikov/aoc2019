const {AsteroidMap} = require('../common/asteroid_map');
const {input} = require('./input');

console.log(new AsteroidMap(input).findBestAsteroid());
