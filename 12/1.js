const {NBodySimulator} = require('../common/nbody');
const {input} = require('./input');

const sim = NBodySimulator.constructFromInput(input);
sim.step(1000);
console.log(sim.energy.total);
