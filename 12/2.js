const {NBodySimulator} = require('../common/nbody');
const {input} = require('./input');

console.log(NBodySimulator.findCycleFromInput(input));
